import time
import logging

import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import applicationConfiguration

class HomePageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In HomePageHandler::get():\n%s", self.request)

        templateData = templateDataManager.getTemplateData(self.request)

        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))

        contestState = 0
        if currentTimeAsEpochTime < templateData['contestStartTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #4a4">starts</span></b> in : ' \
                '<b id="days">00</b> days <b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestStartTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Not Started'''
            contestState = 0
        elif currentTimeAsEpochTime >= templateData['contestEndTimeAsEpochTime']:
            templateData['countDownBoxMarkUp'] = '<b>codathon has <span style="color: #c22">ended</span></b>'
            '''Contest Ended'''
            contestState = 2
        else:
            templateData['countDownBoxMarkUp'] = \
                '<b>codathon <span style="color: #c22">ends</span></b> in : ' \
                '<b id="hours">00</b> hours <b id="minutes">00</b> minutes <b id="seconds">00</b> seconds <script>startCountDown(%s)</script>' \
                % (templateData['contestEndTimeAsEpochTime'] - currentTimeAsEpochTime)
            '''Contest Ongoing'''
            contestState = 1

        logging.info('contestState is: [%s] ' % contestState)

        templateData['contestState'] = '<script>checkContestState(%s)</script>' %contestState

        # Render the page
        self.render_response('homePage.html', **templateData)

