import time
import json
import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import questionDataConfiguration, teamDataConfiguration
from configurationManager import applicationConfiguration
from SQLiteUtil import SQLiteUtil
import Team
import Question

class CheckResultHandler(BaseRequestHandler):
    sqlUtil = SQLiteUtil()
    def checkTeamToken(self):

        _userInputToken = self.request.get('token')
        logging.info('Checking for team %s' % _userInputToken)
        team=sqlUtil.getTeamDetailsByToken(_userInputToken)

        _isSuccess = 'false'
        result = {}
        if team:
            logging.info('Team found for token\n%s' % _userInputToken)
            _isSuccess = 'true'
            result['_teamName'] = team.get_team_name()
            result['_currentQuestion'] = team.get_team_current_question()
        else:
            logging.info('Team NOT found for token\n%s' % _userInputToken)

        result["_isSuccess"]=_isSuccess
        logging.debug('Result is %s'%result)

        self.response.out.write(json.dumps(result))

    def checkUserSolution(self,teamToken):
        _userSolution = self.request.get('usersolution')
        _userQuestionId = self.request.get('_current_question_id')
        logging.debug('_userQuestionId is %s' %_userQuestionId)
        result={}
        _isSuccess = 'true'
        current_question_id = int(_userQuestionId)
        if _userQuestionId and int(_userQuestionId) !=-1:
            logging.info('checkUserSolution: check answer in database')
            _actualSolution = sqlUtil.getAnswerByIdQuery(_userQuestionId)
            if _actualSolution[0] == _userSolution:
                current_question_id = current_question_id +1
                submit_timestamp = self.request.get('submit_timestamp')
                sqlUtil.updateTeamPoints(teamToken,submit_timestamp)
                logging.info('updating team points %s ' %teamToken )
            else:
                _isSuccess = 'false'

        else:
            logging.info('checkUserSolution: load question')
            qid = sqlUtil.getCurrentQuestionOfTeamQuery(teamToken)
            if qid:
                current_question_id = qid[0]
            else:
                current_question_id = -1

            logging.debug('checkUserSolution:_userQuestionId %s' %qid)
        _questionAvailable = 'true'

        team=sqlUtil.getTeamDetailsByToken(teamToken)
        teamName='Team Name Not Found'
        if team:
            teamName = team.get_team_name()

        result['_teamName'] = teamName

        if int(current_question_id) <= int(applicationConfiguration['totalQuestions']):
            question = sqlUtil.getQuestionByIdQuery(current_question_id)
            if question:
                result['_question']=question.get_question()
                logging.debug('question is %s' %question.get_question())
                result['_testCasesPath']=question.get_test_case_file_path()
                result['_testcase']=question.get_test_case_file_path()
        else:
            _questionAvailable = 'false'
        result["_questionAvailable"]=_questionAvailable

        result['_current_question_id'] = current_question_id

        result["_isSuccess"]=_isSuccess

        logging.info('Result %s ' %result)

        self.response.out.write(json.dumps(result))

    def get(self):
        global sqlUtil
        sqlUtil = SQLiteUtil()
        _request = self.request.get('requestType')

        _userInputToken = self.request.get('teamCode')

        _checkTeamTokenRequest = 'userValidation'
        _checkUserSolutionRequest = 'checkUserSolution'

        logging.info('Request type is %s' % _request)

        if _request == _checkTeamTokenRequest:
            self.checkTeamToken()
        elif _request == _checkUserSolutionRequest:
            self.checkUserSolution(_userInputToken)
        else:
            logging.CRITICAL("Incorrect ajax request.. ")
