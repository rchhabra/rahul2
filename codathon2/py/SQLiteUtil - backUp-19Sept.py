import sqlite3
import sys
import logging
from Question import Question
from Team import Team
from configurationManager import applicationConfiguration
import threading

class SQLiteUtil:
	lock = threading.Lock()

	def executeInsertUpdateQuery(self,db,query):
		self.conn = sqlite3.connect(db)
		self.c = self.conn.cursor()
		self.c.execute(query)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()

	def updateTeamPoints(self,teamToken,submit_timestamp):
		self.conn = sqlite3.connect(_dbPath)
		self.c = self.conn.cursor()
		query="update Team set  team_current_question=team_current_question +1, team_score = team_score+10, \
                                     team_total_time_taken="+ submit_timestamp +" where  team_token = '" +teamToken +"'"
		self.c.execute(query)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()

	def getQuestionByIdQuery(self,qid):
		self.conn = sqlite3.connect(_dbPath)
		self.c = self.conn.cursor()
		query="select * from Question where  question_id = " +str(qid) +""
		self.c.execute(query)
		quest=''
		for row in self.c:
		  question_id, question, answer,test_case_file_path  = row
		  quest = Question(question_id, question, answer,test_case_file_path)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		return quest

	def getAnswerByIdQuery(self,qid):
		self.conn = sqlite3.connect(_dbPath)
		self.c = self.conn.cursor()
		query="select answer  from Question where  question_id = " +str(qid) +""
		self.c.execute(query)
		answer =''
		for row in self.c:
		   answer  = row

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		return answer

	def getCurrentQuestionOfTeamQuery(self,teamToken):
		logging.debug('getCurrentQuestionOfTeamQuery invoked')
		self.conn = sqlite3.connect(_dbPath)
		self.c = self.conn.cursor()

		logging.debug('getCurrentQuestionOfTeamQuery teamToken %s' %teamToken)
		query="select team_current_question from Team where  team_token = '" +teamToken +"'"
		print query
		self.c.execute(query)
		team_current_question =""
		for row in self.c:
		  team_current_question = row
		  print row

		print team_current_question

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		logging.debug('getCurrentQuestionOfTeamQuery ended team_current_question')
		return team_current_question

	def getTeamDetailsByNameQuery(self,db,query):
		self.conn = sqlite3.connect(db)
		self.c = self.conn.cursor()
		self.c.execute(query)
		'''self.c.execute("select * from Team where  team_name = '" +query +"'")'''
		team = ""
		for row in self.c:
		  team_name,team_token,team_score,team_current_question,team_total_time_taken  = row
		  team = Team(team_name,team_token,team_score,team_current_question,team_total_time_taken)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		return team

    # For HOME- Login page
	def getTeamDetailsByToken(self,token):
		self.lock.acquire() # critical section
		try:
			self.conn = sqlite3.connect(_dbPath)
			self.c = self.conn.cursor()
			self.c.execute("select * from Team where  team_token = '" +token +"'")
			team = ""
			for row in self.c:
			  team_name,team_token,team_score,team_current_question,team_total_time_taken  = row
			  team = Team(team_name,team_token,team_score,team_current_question,team_total_time_taken)

			self.conn.commit()

    		# We can also close the cursor if we are done with it
			self.c.close()
		finally:
			self.lock.release()
		return team

    # For leadership Page
	def getAllTeamDetails(self):
		self.lock.acquire() # critical section
		try:
			self.conn = sqlite3.connect(_dbPath)
			self.c = self.conn.cursor()
			self.c.execute("SELECT * FROM Team order by team_score desc, team_total_time_taken asc")
			teamList =[]
			for row in self.c:
			  team_name,team_token,team_score,team_current_question,team_total_time_taken  = row
			  team = Team(team_name,team_token,team_score,team_current_question,team_total_time_taken)
			  teamList.append(team)

			self.conn.commit()

    		# We can also close the cursor if we are done with it
			self.c.close()
		finally:
			self.lock.release()
		return teamList

_dbPath= applicationConfiguration['databasePath']
