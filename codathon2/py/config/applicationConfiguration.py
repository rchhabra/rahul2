applicationConfiguration = {
    'hostName':                         'localhost',
    'serverPort':                       8080,
    'helpEmailId':                      'itc-tech-week-2012@company-name.com',
    'registrationLink':                 'mailto:itc-tech-week-2012@company-name.com?Subject=Registration%20for%20codathon%202012',
    'salutationMarkup':                 '<b>India Tech Council</b> presents<br>a <b>Tech Fest 2012</b> event<br>',
    'questionReleaseIntervalInMinutes': 30,
    'numberOfQuestions':                6,
    'contestStartTimeAsEpochTime':      1348063623,
    'contestDurationInMinutes':      2500,
    'isDebugMode':                      True,
    'databasePath':                      'C:\\Users\\rahul\\codathon\\database\\Codathon.sqlite',
    'totalQuestions':                   3,
}
