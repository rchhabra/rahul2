class Question:
    """A simple example class"""
    question_id = 0
    question = ""
    answer = ""
    test_case_file_path = ""
    
    def __init__(self,question_id,question,answer,test_case_file_path):
    	self.question_id = question_id
    	self.question = question
    	self.answer = answer
    	self.test_case_file_path = test_case_file_path
    	
    def get_question_id(self):
        return self.question_id
        
    def get_question(self):
        return self.question
        
    def get_answer(self):
        return self.answer
        
    def get_test_case_file_path(self):
        return self.test_case_file_path
    