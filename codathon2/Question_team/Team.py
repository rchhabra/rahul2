class Team:
    """A simple Team class"""
    team_name = ""
    team_token = ""
    team_score = ""
    team_current_question = ""
    team_total_time_taken = 0
    
    def __init__(self,team_name,team_token,team_score,team_current_question,team_total_time_taken):
    	self.team_name = team_name
    	self.team_token = team_token
    	self.team_score = team_score
    	self.team_current_question = team_current_question
    	self.team_total_time_taken = team_total_time_taken
    	
    def get_team_name(self):
        return self.team_name
        
    def get_team_token(self):
        return self.team_token
        
    def get_team_score(self):
        return self.team_score
        
    def get_team_current_question(self):
        return self.team_current_question
        
    def get_team_total_time_taken(self):
        return self.team_total_time_taken
    